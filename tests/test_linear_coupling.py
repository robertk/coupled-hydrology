import dataclasses

import numpy as np

from linear_model.analysis import compute_coupling_behavior
from linear_model.process_results import compute_convergence_rate
from linear_model.setup_simulation import BoundaryCondition, InitialCondition, Params
from linear_model.simulation import run_coupled_simulation

params = Params(
    tolerance=1e-3,
    max_iterations=15,
    min_iterations=2,
    omega=1,
    coupling_scheme="serial-implicit",
    K=1.0,
    c=1.0,
    L=1.0,
    h_0=1.0,
    t_0=0,
    t_end=0.1,
    N=1,
    M=20,
    bc_type="dirichlet",
    dirichlet_value=0.0,
    ic_type="linear",
    precice_config_template="linear_model/precice-config.xml.j2",
    precice_config="linear_model/precice-config.xml",
)


def test_full_coupling():
    run_coupled_simulation(params)

    cvg_logfile = "precice-RiverSolver-convergence.log"
    convergence_rate = compute_convergence_rate(cvg_logfile)

    _, S, omega_opt = compute_coupling_behavior(
        params.K, params.c, params.L, params.dt, params.M
    )
    assert abs(abs(S) - convergence_rate) < 1e-8

    run_coupled_simulation(dataclasses.replace(params, omega=omega_opt))
    optimal_cvg_rate = compute_convergence_rate(cvg_logfile)
    assert optimal_cvg_rate < 1e-8


def test_bc_sensitivity():
    cvg_logfile = "precice-RiverSolver-convergence.log"

    S = compute_coupling_behavior(params.K, params.c, params.L, params.dt, params.M)[1]
    analytical_cvg_rate = abs(S)

    run_coupled_simulation(
        dataclasses.replace(
            params, bc_type=BoundaryCondition.dirichlet, dirichlet_value=0.0
        )
    )
    cvg_rate_hom_dbc = compute_convergence_rate(cvg_logfile)

    run_coupled_simulation(
        dataclasses.replace(
            params, bc_type=BoundaryCondition.dirichlet, dirichlet_value=10.0
        )
    )
    cvg_rate_nhom_dbc = compute_convergence_rate(cvg_logfile)

    run_coupled_simulation(
        dataclasses.replace(params, bc_type=BoundaryCondition.no_flux)
    )
    cvg_rate_no_flux = compute_convergence_rate(cvg_logfile)

    run_coupled_simulation(
        dataclasses.replace(params, bc_type=BoundaryCondition.free_drainage)
    )
    cvg_rate_free_drainage = compute_convergence_rate(cvg_logfile)

    assert abs(cvg_rate_hom_dbc - analytical_cvg_rate) < 1e-8
    assert abs(cvg_rate_nhom_dbc - analytical_cvg_rate) < 1e-8
    assert (
        abs((cvg_rate_free_drainage - analytical_cvg_rate) / analytical_cvg_rate) < 1e-1
    )
    assert abs((cvg_rate_no_flux - analytical_cvg_rate) / analytical_cvg_rate) < 1e-1


def test_bc_sensitivity_grid_size():
    cvg_logfile = "precice-RiverSolver-convergence.log"

    S = compute_coupling_behavior(params.K, params.c, params.L, params.dt, params.M)[1]
    analytical_cvg_rate = abs(S)

    run_coupled_simulation(
        dataclasses.replace(params, bc_type=BoundaryCondition.no_flux)
    )
    cvg_rate_no_flux = compute_convergence_rate(cvg_logfile)

    run_coupled_simulation(
        dataclasses.replace(params, bc_type=BoundaryCondition.free_drainage)
    )
    cvg_rate_free_drainage = compute_convergence_rate(cvg_logfile)

    error_coarse_fd = abs(cvg_rate_free_drainage - analytical_cvg_rate)
    error_coarse_nf = abs(cvg_rate_no_flux - analytical_cvg_rate)

    factor = 5
    fine_params = dataclasses.replace(params, M=factor * params.M)
    assert np.allclose(fine_params.dz, 1 / factor * params.dz)

    S = compute_coupling_behavior(
        fine_params.K, fine_params.c, fine_params.L, fine_params.dt, fine_params.M
    )[1]
    analytical_cvg_rate = abs(S)

    run_coupled_simulation(
        dataclasses.replace(fine_params, bc_type=BoundaryCondition.no_flux)
    )
    cvg_rate_no_flux = compute_convergence_rate(cvg_logfile)

    run_coupled_simulation(
        dataclasses.replace(fine_params, bc_type=BoundaryCondition.free_drainage)
    )
    cvg_rate_free_drainage = compute_convergence_rate(cvg_logfile)

    error_fine_fd = abs(cvg_rate_free_drainage - analytical_cvg_rate)
    error_fine_nf = abs(cvg_rate_no_flux - analytical_cvg_rate)

    assert (error_fine_fd - error_coarse_fd) < 1e-4
    assert (error_fine_nf - error_coarse_nf) < 1e-4


def test_ic_sensitivity():
    cvg_logfile = "precice-RiverSolver-convergence.log"

    S = compute_coupling_behavior(params.K, params.c, params.L, params.dt, params.M)[1]
    analytical_cvg_rate = abs(S)

    assert params.ic_type == InitialCondition.linear
    run_coupled_simulation(params)
    cvg_rate_linear = compute_convergence_rate(cvg_logfile)

    run_coupled_simulation(dataclasses.replace(params, ic_type=InitialCondition.sin))
    cvg_rate_sin = compute_convergence_rate(cvg_logfile)

    assert abs(cvg_rate_linear - analytical_cvg_rate) < 1e-8
    assert abs(cvg_rate_sin - analytical_cvg_rate) < 1e-8
