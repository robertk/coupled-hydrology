import ufl
from dune.fem.operator import linear

from linear_model.analysis import get_a, get_b
from linear_model.setup_simulation import Params
from linear_model.subsurface import Groundwater


def _grad_based_flux(groundwater: Groundwater):
    grad_psi_sfc = groundwater.space.interpolate(ufl.grad(groundwater.psi_h)).as_numpy[
        -1
    ]
    dune_flux = -groundwater.K.value * (grad_psi_sfc + 1)
    return dune_flux


def test_flux_computation():
    params = Params(
        K=1.0,
        c=1.0,
        L=1.0,
        h_0=1.0,
        t_0=0,
        t_end=0.1,
        N=1,
        M=20,
        bc_type="dirichlet",
        dirichlet_value=0.0,
        ic_type="linear",
    )
    groundwater = Groundwater(params)
    gw_flux = groundwater.interface_flux
    grad_based_flux = _grad_based_flux(groundwater)
    assert gw_flux * grad_based_flux > 0
    assert abs(gw_flux - grad_based_flux) < 0.1


def test_matrix():
    params = Params(
        K=1.0,
        c=1.0,
        L=1.0,
        h_0=1.0,
        t_0=0,
        t_end=0.1,
        N=1,
        M=20,
        bc_type="dirichlet",
        dirichlet_value=0.0,
        ic_type="linear",
    )
    groundwater = Groundwater(params)
    system_matrix = linear(groundwater.scheme).as_numpy
    a = get_a(params.K, params.c, params.dt, params.dz)
    b = get_b(params.K, params.c, params.dt, params.dz)
    assert abs(system_matrix[1, 1] - a) < 1e-8
    assert abs(system_matrix[1, 2] - b) < 1e-8
    assert abs(system_matrix[2, 1] - b) < 1e-8
    assert abs(system_matrix[-2, -2] - a) < 1e-8
    assert abs(system_matrix[-3, -2] - b) < 1e-8
    assert abs(system_matrix[-2, -3] - b) < 1e-8
    assert abs(system_matrix[-1, -1] - 1) < 1e-8
