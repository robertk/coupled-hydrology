from linear_model.setup_simulation import Params
from linear_model.surface import River


def test_steady_state():
    params = Params(
        K=1.0,
        c=1.0,
        L=1.0,
        h_0=1.0,
        t_0=0,
        t_end=0.1,
        N=1,
        M=20,
        bc_type="dirichlet",
        dirichlet_value=0.0,
        ic_type="linear",
    )
    river = River(params)
    height_before = river.height
    river.solve(dt=1, flux=0)
    assert river.height == height_before
