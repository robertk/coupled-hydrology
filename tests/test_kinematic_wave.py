import dataclasses

import numpy as np

from coupling.params import Params
from coupling.sfc_kinematic_wave import (
    FlatSurface,
    GaussianBump,
    KinematicWaveSurface,
    OneSidedOutflow,
    Outflow,
    standalone_surface,
)
from experiments.hillslope.utils import analytical_solution, rain_flux
from postprocessing.plot_surface import load_surface

Lx = 1.0
t_0 = 0
t_end = 0.5

N = 100
Mx = 30

output_directory = "."
output_period = 0

base_params = Params(
    t_0=t_0,
    t_end=t_end,
    N=N,
    Mx=Mx,
    Lx=Lx,
    output_directory=output_directory,
    output_period=output_period,
    surface_slope=0.0005,
    mannings_coefficient=0.02,
)


def test_steady_state():
    initial_condition = FlatSurface(1.0)
    boundary_condition = Outflow()
    surface = KinematicWaveSurface(base_params, boundary_condition, initial_condition)
    state_before = surface.state.as_numpy
    surface.step()
    state_after = surface.state.as_numpy
    assert abs(state_after - state_before).max() == 0


def test_convergence():
    convergence_params = dataclasses.replace(base_params, Lx=5.0, t_end=0.5, Mx=500)
    boundary_condition = Outflow()
    initial_condition = GaussianBump(convergence_params)

    def time_loop():
        t = convergence_params.t_0
        while t < convergence_params.t_end:
            surface.step()
            surface.end_time_step()
            t += convergence_params.dt

    surface = KinematicWaveSurface(
        convergence_params, boundary_condition, initial_condition
    )
    time_loop()
    height_coarse = surface.state.as_numpy[::2]
    velocity_coarse = surface.state.as_numpy[1::2] / height_coarse

    convergence_params.Mx *= 2
    surface = KinematicWaveSurface(
        convergence_params, boundary_condition, initial_condition
    )
    time_loop()
    height_fine = surface.state.as_numpy[::4]
    velocity_fine = surface.state.as_numpy[1::4] / height_fine

    assert height_fine.shape == height_coarse.shape
    assert velocity_coarse.shape == velocity_fine.shape

    tolerance = 1e-2
    assert np.mean(abs(height_coarse - height_fine)) < tolerance
    assert np.mean(abs(velocity_coarse - velocity_fine)) < tolerance


def test_1d_channel(tmpdir):
    coarse_dir = tmpdir.mkdir("coarse")
    fine_dir = tmpdir.mkdir("fine")
    params = Params(
        t_0=0,
        t_end=18000,
        N=100,
        output_period=1,
        Mx=5,
        Lx=400,
        surface_slope=0.0005,
        mannings_coefficient=0.02,
        output_directory=coarse_dir,
    )
    initial_condition = FlatSurface(0.0)
    boundary_condition = OneSidedOutflow(params)

    v_ana = np.vectorize(lambda t: analytical_solution(t, params))
    t = np.linspace(params.t_0, params.t_end, int(params.N / params.output_period) + 1)
    analytical = v_ana(t)

    standalone_surface(params, initial_condition, boundary_condition, rain_flux)
    h, v = load_surface(params)
    outflow_coarse = (h * v)[:, 0]
    error_coarse = np.abs(analytical - outflow_coarse)

    assert error_coarse.max() < 0.26 * analytical.max()

    params = dataclasses.replace(
        params,
        Mx=400,
        output_directory=fine_dir,
    )
    standalone_surface(params, initial_condition, boundary_condition, rain_flux)
    h, v = load_surface(params)
    outflow_fine = (h * v)[:, 0]
    error_fine = np.linalg.norm(analytical - outflow_fine)

    assert error_fine.max() < error_coarse.max()
    assert error_fine.max() < 0.17 * analytical.max()
