import numpy as np

import coupling.params


def test_load_params():
    coupling.params.load_params("tests/params.yaml")


def test_position_computation():
    params = coupling.params.load_params("tests/params.yaml")
    assert len(params.positions_gnd) == len(params.positions_sfc) + 1
    average_positions_gnd = 0.5 * (
        np.array(params.positions_gnd[:-1]) + np.array(params.positions_gnd[1:])
    )
    assert np.allclose(average_positions_gnd, np.array(params.positions_sfc))
