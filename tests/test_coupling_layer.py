import numpy as np
import ufl
from dune.fem.space import finiteVolume
from dune.grid import structuredGrid

from coupling.coupling_layer import KinWaveCouplingLayer, ShallowWaterCouplingLayer
from coupling.params import Params

params = Params(
    t_0=0,
    t_end=1.0,
    N=1,
    Mx=10,
    Mz=1,
    Lx=1,
    Lz=1,
    output_directory=".",
    output_period=0,
)


def test_fv_to_fem_mapping_kinwave():
    cpl_layer = KinWaveCouplingLayer(params)
    grid = structuredGrid([0], [params.Lx], [params.Mx])
    fv_space = finiteVolume(grid)
    x = ufl.SpatialCoordinate(fv_space)[0]
    fv_state = fv_space.interpolate(x, "fv_state")
    mapped_state = cpl_layer.mapped_river_height(fv_state)

    fv_array = fv_state.as_numpy
    averages = 0.5 * (fv_array[:-1] + fv_array[1:])
    explicit_mapping = np.array([fv_array[0], *averages, fv_array[-1]])
    assert np.allclose(mapped_state, explicit_mapping)


def test_fv_to_fem_mapping_swe():
    cpl_layer = ShallowWaterCouplingLayer(params)
    grid = structuredGrid([0], [params.Lx], [params.Mx])
    fv_space = finiteVolume(grid, dimRange=2)
    x = ufl.SpatialCoordinate(fv_space)[0]
    fv_state = fv_space.interpolate([x, 0], "fv_state")
    mapped_state = cpl_layer.mapped_river_height(fv_state)

    fv_array = fv_state.as_numpy[::2]
    averages = 0.5 * (fv_array[:-1] + fv_array[1:])
    explicit_mapping = np.array([fv_array[0], *averages, fv_array[-1]])
    assert np.allclose(mapped_state, explicit_mapping)
