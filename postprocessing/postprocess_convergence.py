from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd


def compute_convergence_factors(convergence_log: Path) -> float:
    df = pd.read_csv(convergence_log, sep=r"\s+")
    time_windows = df["TimeWindow"].drop_duplicates().to_numpy()
    cvg_factors = np.zeros(len(time_windows))
    for index, tw in enumerate(time_windows):
        resabs_values = df.loc[df["TimeWindow"] == tw]["ResAbs(h)"].to_numpy()
        if len(resabs_values) == 1:
            cvg_factors[index] = np.nan
            continue
        cvg_factors[index] = np.mean(resabs_values[1:] / resabs_values[:-1])

    cvg_factors = np.ma.masked_invalid(cvg_factors)

    return cvg_factors


def compute_mean_convergence_factor(convergence_log: Path) -> float:
    df = pd.read_csv(convergence_log, sep=r"\s+")
    time_windows = df["TimeWindow"].drop_duplicates().to_numpy()
    cvg_factors = np.zeros(len(time_windows))
    for index, tw in enumerate(time_windows):
        resabs_values = df.loc[df["TimeWindow"] == tw]["ResAbs(h)"].to_numpy()
        if len(resabs_values) == 1:
            cvg_factors[index] = np.nan
            continue
        cvg_factors[index] = np.mean(resabs_values[1:] / resabs_values[:-1])

    cvg_factors = np.ma.masked_invalid(cvg_factors)
    mean_cvg_factor = np.mean(cvg_factors)
    return mean_cvg_factor


def compute_final_convergence_factor(convergence_log: Path) -> float:
    df = pd.read_csv(convergence_log, sep=r"\s+")
    time_windows = df["TimeWindow"].drop_duplicates().to_numpy()
    resabs_values = df.loc[df["TimeWindow"] == time_windows[-1]]["ResAbs(h)"].to_numpy()
    if len(resabs_values) == 1:
        return np.nan
    else:
        return np.mean(resabs_values[1:] / resabs_values[:-1])


def compute_first_convergence_factor(convergence_log: Path) -> float:
    df = pd.read_csv(convergence_log, sep=r"\s+")
    time_windows = df["TimeWindow"].drop_duplicates().to_numpy()
    resabs_values = df.loc[df["TimeWindow"] == time_windows[0]]["ResAbs(h)"].to_numpy()
    if len(resabs_values) == 1:
        return np.nan
    else:
        return np.mean(resabs_values[1:] / resabs_values[:-1])


def postprocess_convergence_factor(
    convergence_log: Path, create_plot: bool = True
) -> float:
    df = pd.read_csv(convergence_log, sep=r"\s+")
    time_windows = df["TimeWindow"].drop_duplicates().to_numpy()
    cvg_factors = np.zeros(len(time_windows))
    for index, tw in enumerate(time_windows):
        resabs_values = df.loc[df["TimeWindow"] == tw]["ResAbs(h)"].to_numpy()
        if len(resabs_values) == 1:
            cvg_factors[index] = np.nan
            continue
        cvg_factors[index] = np.mean(resabs_values[1:] / resabs_values[:-1])

    cvg_factors = np.ma.masked_invalid(cvg_factors)

    if create_plot:
        plt.style.use("stylesheet.mpl")
        fig, ax = plt.subplots()
        ax.scatter(time_windows, cvg_factors, color="k", marker=".")
        ax.set(
            title="Convergence factor over time",
            xlabel="Time Window",
            ylabel=r"Measured $\Sigma$",
        )
        fig.savefig("convergence_factor.png")
    mean_cvg_factor = np.mean(cvg_factors)
    # print in boldface
    print("\033[1m" + f"Measured Convergence Factor: {mean_cvg_factor}" + "\033[0m")
    return mean_cvg_factor
