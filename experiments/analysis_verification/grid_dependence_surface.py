from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
from matplotlib import ticker

from linear_model.analysis import compute_coupling_behavior

plt.style.use("stylesheet.mpl")
L = 10

dt_values = np.logspace(-5, 1, 20)
dz_values = np.logspace(-5, 1, 20)

c = 1
K = 1

if __name__ == "__main__":
    plotting_dir = Path("plots/analysis_verification")
    plotting_dir.mkdir(parents=True, exist_ok=True)
    # plotting_dir = Path(
    #     "/home/valentina/Nextcloud/PhD/Richards-SWE/richards-swe-paper/plots/"
    # )

    omega_opt = np.zeros((len(dt_values), len(dz_values)))

    dt_grid, dz_grid = np.meshgrid(dt_values, dz_values)

    np.vectorize(compute_coupling_behavior)

    M_grid = np.round(L / dz_grid)
    _, S, omega = compute_coupling_behavior(K, c, L, dt_grid, M_grid)
    assert np.all(S < 0)

    fig, ax = plt.subplots(layout="constrained")
    fig.set(figwidth=3, figheight=3)
    m = ax.contourf(
        dt_grid,
        dz_grid,
        omega,
        cmap="rocket_r",
        levels=np.linspace(0, 1, 11),
    )
    ax.set(
        xlabel=r"$\Delta t$",
        ylabel=r"$\Delta z$",
        xscale="log",
        yscale="log",
    )
    fig.colorbar(m, label=r"$\omega_\mathrm{opt}$", ax=ax)
    fig.savefig(plotting_dir / "grid_dependence_omega.pdf")

    fig, ax = plt.subplots(layout="constrained")
    fig.set(figwidth=3.5, figheight=3)
    cs = ax.contourf(
        dt_grid,
        dz_grid,
        -S,
        cmap="rocket_r",
        locator=ticker.LogLocator(),
        levels=np.logspace(-4, 3, 8),
    )
    cs2 = ax.contour(
        cs,
        levels=cs.levels[::4],
        locator=ticker.LogLocator(),
        colors="w",
    )
    ax.set(
        xlabel=r"$\Delta t$",
        ylabel=r"$\Delta z$",
        xscale="log",
        yscale="log",
    )
    cbar = fig.colorbar(cs, label=r"$|S|$", ax=ax)
    cbar.add_lines(cs2)
    fig.savefig(plotting_dir / "grid_dependence_factor.pdf")
