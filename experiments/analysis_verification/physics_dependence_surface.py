from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
from matplotlib import ticker

from linear_model.analysis import compute_coupling_behavior

plt.style.use("stylesheet.mpl")
L = 1
N = 1
t_end = 0.1
t_start = 0.0
dt = (t_end - t_start) / N

c_values = 10.0 ** np.linspace(-3, 3, 50)
K_values = 10.0 ** np.linspace(-3, 3, 30)
grid_sizes = np.array([1 / 20, 1 / 500])

if __name__ == "__main__":
    plotting_dir = Path("plots/analysis_verification")
    plotting_dir.mkdir(parents=True, exist_ok=True)
    # plotting_dir = Path(
    #     "/home/valentina/Nextcloud/PhD/Richards-SWE/richards-swe-paper/plots/"
    # )

    omega_opt = np.zeros((len(grid_sizes), len(c_values), len(K_values)))
    S_abs = omega_opt.copy()
    CR = omega_opt.copy()

    c_grid, K_grid = np.meshgrid(c_values, K_values)

    np.vectorize(compute_coupling_behavior)

    grid_size_str = ["1/20", "1/500"]
    for g_index, grid_size in enumerate(grid_sizes):
        dz = grid_size
        M = round(L / grid_size)
        _, S, omega = compute_coupling_behavior(K_grid, c_grid, L, dt, M)

        assert np.all(S < 0)

        fig, ax = plt.subplots(layout="constrained")
        fig.set(figwidth=3, figheight=3)
        ax.contourf(
            c_grid,
            K_grid,
            omega,
            cmap="rocket_r",
            levels=np.linspace(0, 1, 11),
        )
        ax.set(
            xscale="log",
            yscale="log",
            xlim=[1e-3, 1e3],
            ylim=[1e-3, 1e3],
            xlabel="$c$",
            ylabel="$K$",
        )
        fig.savefig(plotting_dir / f"physics_dependence_omega_{grid_size}.pdf")

        fig, ax = plt.subplots(layout="constrained")
        fig.set(figwidth=3, figheight=3)
        cs = ax.contourf(
            c_grid,
            K_grid,
            -S,
            cmap="rocket_r",
            locator=ticker.LogLocator(),
            levels=np.logspace(-4, 3, 8),
        )
        cs2 = ax.contour(
            cs,
            levels=cs.levels[::4],
            locator=ticker.LogLocator(),
            colors="w",
        )
        ax.set(
            xscale="log",
            yscale="log",
            xlim=[1e-3, 1e3],
            ylim=[1e-3, 1e3],
            xlabel="$c$",
            ylabel="$K$",
        )
        fig.savefig(plotting_dir / f"physics_dependence_factor_{grid_size}.pdf")
