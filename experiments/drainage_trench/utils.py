import dataclasses

import ufl
from dune.ufl import Constant, DirichletBC

import coupling.ground as gnd
from coupling.params import Params


class CouplingBenchmark(gnd.GroundDirichletBC):
    def __init__(self, params: Params):
        self.water_table = Constant(1, name="water_table")
        self.Lx = params.Lx
        self.Lz = params.Lz

    def get_dirichlet_bc(self, space, t) -> list[DirichletBC]:
        x, z = ufl.SpatialCoordinate(space)
        bc_left = DirichletBC(
            space,
            self.water_table - z,
            ufl.conditional(x > (self.Lx.value - 1e-8), 1, 0)
            * ufl.conditional(z < self.water_table, z - self.water_table, 0),
        )
        bc_right = DirichletBC(
            space,
            self.water_table - z,
            ufl.conditional(x < 1e-8, 1, 0)
            * ufl.conditional(z < self.water_table, z - self.water_table, 0),
        )
        return [bc_left, bc_right]


def get_schneid_params(**kwargs):
    """Get parameters for Schneid benchmarks.

    Returns a default set of parameters for loam.
    Keyword arguments can be used to replace individual values (e.g., `Lz=10.0`).

    :return: parameters for simulations on a hillslope
    :rtype: Params
    """
    params_dict = {
        "tolerance": 1e-8,
        "max_iterations": 15,
        "min_iterations": 2,
        "omega": 1,
        "coupling_scheme": "serial-implicit",
        "soil_type": "loam",
        "Lx": 2.0,
        "Lz": 3.0,
        "t_0": 0,
        "Mx": 20,
        "Mz": 30,  # corresponds to 21x31 = 651 nodes (same as in List & Radu, 2016)
        "precice_config_template": "coupling/precice-config.xml.j2",
        "precice_config": "coupling/precice-config.xml",
        "output_directory": "output",
        "output_period": 1,
        "t_end": (3 / 16) * 86400,
        "N": 3,
    }

    params = Params(**params_dict)
    return dataclasses.replace(params, **kwargs)


def rain_flux(
    t: ufl.Constant,
    x: ufl.SpatialCoordinate,
    rate: float = 3.3e-4 / 60,
    cutoff: int = 12000,
):
    """Compute precipitation flux.

    Spatially uniform precipitation, equal to `rate` until `t == cutoff`, 0 afterwards.

    :param t: time
    :type t: ufl.Constant
    :param x: space (unused)
    :type x: ufl.SpatialCoordinate
    :param rate: Rain rate in m/s, defaults to 3.3*1e-4/60
    :type rate: float, optional
    :param cutoff: Cutoff time in s, defaults to 12000
    :type cutoff: float, optional
    :return: Precipitation flux in m/s
    :rtype: ufl.Conditional
    """
    return ufl.conditional(t < cutoff, rate, 0.0)
