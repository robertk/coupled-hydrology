from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np

from coupling.soil import Soil, beit_netofa_clay, gravel, sand, silt_loam, sandy_loam_4, sandy_loam_5, sandy_loam_6

plt.style.use("stylesheet.mpl")
plotting_dir = Path("plots/soil_properties")
plotting_dir.mkdir(parents=True, exist_ok=True)


def capacity(psi: float, soil: Soil):
    alpha = soil.alpha
    theta_S = soil.theta_S
    theta_R = soil.theta_R
    nVG = soil.nVG
    C = (
        alpha
        * (theta_S - theta_R)
        * (nVG - 1)
        * (-alpha * psi) ** (nVG - 1)
        * (1 + (-alpha * psi) ** nVG) ** (1 / nVG - 2)
    )
    C[psi > 0] = 0.0
    return C


def conductivity(psi: float, soil: Soil):
    nVG = soil.nVG
    m = 1 - 1 / nVG
    alpha = soil.alpha
    Ks = soil.Ks
    S = (1 + (-alpha * psi) ** nVG) ** (-m)
    K = Ks * S**0.5 * (1 - (1 - S ** (1 / m)) ** m) ** 2
    K[psi > 0] = Ks
    return K

def determine_max_values(psi: float, soil: Soil):
    c = capacity(psi, soil)
    K = conductivity(psi, soil)
    print(f"Max c: {np.max(c):.3g} at {psi[np.argmax(c)]:.3g}")
    print(f"Max K: {np.max(K):.3g} at {psi[np.argmax(K)]:.3g}")
    cK_index = np.argmax(c*K)
    print(f"Max cK: {np.max(c*K):.3g} at {psi[cK_index]:.3g}, {c[cK_index]:.3g}, {K[cK_index]:.3g}")


if __name__ == "__main__":
    fig, ax = plt.subplots()
    psi = np.array([0, *(np.logspace(-5, 8, 100))])
    c_clay = capacity(-psi, beit_netofa_clay)
    ax.plot(psi, c_clay, label="clay")
    c_loam = capacity(-psi, silt_loam)
    ax.plot(psi, c_loam, label="loam")
    c_gravel = capacity(-psi, gravel)
    ax.plot(psi, c_gravel, label="gravel")
    c_sand = capacity(-psi, sand)
    ax.plot(psi, c_sand, label="sand")
    ax.set(
        xscale="log",
        title=r"Soil water capacity $c(\psi)$ for different soils",
        xlabel=r"$-\psi$",
        ylabel=r"$c(\psi)$",
        yscale="log",
    )
    ax.legend()
    fig.savefig(plotting_dir / "capacity.pdf")

    fig, ax = plt.subplots()
    K_clay = conductivity(-psi, beit_netofa_clay)
    ax.plot(psi, K_clay, label="clay")
    K_loam = conductivity(-psi, silt_loam)
    ax.plot(psi, K_loam, label="loam")
    K_gravel = conductivity(-psi, gravel)
    ax.plot(psi, K_gravel, label="gravel")
    K_sand = conductivity(-psi, sand)
    ax.plot(psi, K_sand, label="sand")
    ax.set(
        xscale="log",
        title=r"Hydraulic conductivity $K(\psi)$ for different soils",
        xlabel=r"$-\psi$",
        ylabel=r"$K(\psi)$",
        yscale="log",
    )
    ax.legend()
    fig.savefig(plotting_dir / "conductivity.pdf")

    fig, ax = plt.subplots()
    ax.plot(psi, c_clay * K_clay, label="clay")
    ax.plot(psi, c_loam * K_loam, label="loam")
    ax.plot(psi, c_gravel * K_gravel, label="gravel")
    ax.plot(psi, c_sand * K_sand, label="sand")
    ax.set(
        xscale="log",
        title=r"Product $c(\psi) \cdot K(\psi)$ for different soils",
        xlabel=r"$-\psi$",
        ylabel=r"$c\cdot K$",
        yscale="log",
    )
    ax.legend()
    fig.savefig(plotting_dir / "product.pdf")

    fig, ax = plt.subplots()
    ax.plot(psi, c_clay / K_clay, label="clay")
    ax.plot(psi, c_loam / K_loam, label="loam")
    ax.plot(psi, c_gravel / K_gravel, label="gravel")
    ax.plot(psi, c_sand / K_sand, label="sand")
    ax.set(
        xscale="log",
        title=r"Ratio $\frac{c(\psi)}{K(\psi)}$ for different soils",
        xlabel=r"$-\psi$",
        ylabel=r"$c / K$",
        yscale="log",
    )
    ax.legend()
    fig.savefig(plotting_dir / "ratio.pdf")

    print("Beit-Netofa Clay:")
    determine_max_values(-psi, beit_netofa_clay)
    print("Silt Loam:")
    determine_max_values(-psi, silt_loam)
    print("Sandy Loam 4:")
    determine_max_values(-psi, sandy_loam_4)
    print("Sandy Loam 5:")
    determine_max_values(-psi, sandy_loam_5)
    print("Sandy Loam 6:")
    determine_max_values(-psi, sandy_loam_6)