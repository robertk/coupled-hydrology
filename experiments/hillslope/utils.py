import dataclasses

import numpy as np
import ufl
from dune.ufl import DirichletBC
from scipy.optimize import fsolve

from coupling.ground import GroundDirichletBC
from coupling.params import Params


class NoDirichlet(GroundDirichletBC):
    def get_dirichlet_bc(self, space, t):
        return []


class RainDirichlet(GroundDirichletBC):
    def __init__(self, params: Params):
        self.Lz = params.Lz

    def get_dirichlet_bc(self, space, t):
        x, z = ufl.SpatialCoordinate(space)
        rain_bc = DirichletBC(
            space,
            rain_flux(t, x),
            ufl.conditional(z > (self.Lz.value - 1e-8), 1, 0),
        )
        return [rain_bc]


def hillslope_transform(
    x: ufl.SpatialCoordinate, z_shift: float = 0.2, bed_slope: float = 5e-4
):
    """Coordinate transformation for the hillslope test case.

    :param x: 2D coordinate on a cartesion domain
    :type x: ufl.SpatialCoordinate
    :param z_shift: vertical shift of the left edge of the domain, defaults to 0.2
    :type z_shift: float, optional
    :param bed_slope: slope of the hill, defaults to 5e-4
    :type bed_slope: float, optional
    :return: transformed coordinate (from `ufl.as_vector()`)
    """
    return ufl.as_vector([x[0], x[1] + z_shift - bed_slope * x[0]])


def analytical_solution(t, params: Params, cutoff=12000, rain_rate=3.3e-4 / 60):
    alpha = np.sqrt(abs(params.surface_slope)) / params.mannings_coefficient
    L = params.Lx.value
    m = 5 / 3
    i_e = rain_rate
    t_c = (L / (alpha * i_e ** (m - 1))) ** (1 / m)
    if t <= t_c:
        return alpha * (i_e * t) ** m
    if t_c < t <= cutoff:
        return alpha * (i_e * t_c) ** m

    def decay_regime(q):
        return (
            i_e * L - i_e * m * alpha ** (1 / m) * q ** ((m - 1) / m) * (t - cutoff) - q
        )

    return fsolve(decay_regime, 0.0)


def rain_flux(
    t: ufl.Constant,
    x: ufl.SpatialCoordinate,
    rate: float = 3.3e-4 / 60,
    cutoff: int = 12000,
):
    """Compute precipitation flux.

    Spatially uniform precipitation, equal to `rate` until `t == cutoff`, 0 afterwards.

    :param t: time
    :type t: ufl.Constant
    :param x: space (unused)
    :type x: ufl.SpatialCoordinate
    :param rate: Rain rate in m/s, defaults to 3.3*1e-4/60
    :type rate: float, optional
    :param cutoff: Cutoff time in s, defaults to 12000
    :type cutoff: float, optional
    :return: Precipitation flux in m/s
    :rtype: ufl.Conditional
    """
    return ufl.conditional(t < cutoff, rate, 0.0)


def get_hillslope_params(**kwargs):
    """Get parameters for simulations on a hillslope.

    Returns a default set of parameters.
    Keyword arguments can be used to replace individual values (e.g., `Lz=10.0`).

    :return: parameters for simulations on a hillslope
    :rtype: Params
    """
    params_dict = {
        "tolerance": 1e-8,
        "max_iterations": 15,
        "min_iterations": 2,
        "omega": 1,
        "coupling_scheme": "serial-implicit",
        "soil_type": "sandy_loam_5",
        "Lx": 400.0,
        "Lz": 5.0,
        "t_0": 0,
        "t_end": 18000,
        "N": 100,
        "Mx": 5,
        "Mz": 25,
        "precice_config_template": "coupling/precice-config.xml.j2",
        "precice_config": "coupling/precice-config.xml",
        "output_directory": "output",
        "output_period": 1,
        "mannings_coefficient": 3.31e-3 * 60,
        "surface_slope": 5e-4,
    }

    params = Params(**params_dict)
    return dataclasses.replace(params, **kwargs)
