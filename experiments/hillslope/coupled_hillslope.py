import dataclasses
import itertools
from multiprocessing import Process
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
from utils import NoDirichlet, get_hillslope_params, hillslope_transform, rain_flux

from coupling.coupling_layer import GroundCouplingLayer, KinWaveCouplingLayer
from coupling.ground import LinearProfile, simulate_ground
from coupling.params import Params, render
from coupling.sfc_kinematic_wave import FlatSurface, OneSidedOutflow, simulate_surface
from linear_model.analysis import compute_coupling_behavior
from postprocessing.plot_richards import load_data
from postprocessing.plot_surface import load_surface
from postprocessing.postprocess_convergence import compute_convergence_factors

plt.style.use("stylesheet.mpl")
plotting_dir = Path("plots/nonlinear_code")
plotting_dir.mkdir(parents=True, exist_ok=True)


def run_coupled_simulation(params: Params, low_qr=False) -> None:
    render(params)
    coupling_layer = GroundCouplingLayer(params)
    initial_condition = LinearProfile(1, params)
    boundary_condition = NoDirichlet()
    groundwater_proc = Process(
        target=simulate_ground,
        args=[
            params,
            coupling_layer,
            initial_condition,
            boundary_condition,
            lambda x: hillslope_transform(x, bed_slope=params.surface_slope),
        ],
    )
    coupling_layer = KinWaveCouplingLayer(params)
    initial_condition = FlatSurface(0.0)
    boundary_condition = OneSidedOutflow(params)
    if low_qr:
        qr = 3.3e-5 / 60
    else:
        qr = 3.3e-4 / 60
    surface_proc = Process(
        target=simulate_surface,
        args=[
            params,
            initial_condition,
            boundary_condition,
            coupling_layer,
            lambda t, x: rain_flux(t, x, rate=qr),
        ],
    )
    groundwater_proc.start()
    surface_proc.start()
    groundwater_proc.join()
    surface_proc.join()


def sandy_loam_experiments(rerun_experiments=True):
    soil_types = ["sandy_loam_4", "sandy_loam_5", "sandy_loam_6"]
    fig, ax = plt.subplots(layout="constrained")
    fig2, ax2 = plt.subplots(layout="constrained")
    base_params = get_hillslope_params(N=300, output_period=10)
    base_dir = Path(base_params.output_directory)
    convergence_log = Path("precice-Surface-convergence.log")
    ccycle = itertools.cycle(["C0", "C1", "C2"])

    lcycle = itertools.cycle(
        [
            r"$K_s=6.94\times 10^{-4}$ m/min",
            r"$K_s=6.94\times 10^{-5}$ m/min",
            r"$K_s=6.94\times 10^{-6}$ m/min",
        ]
    )

    for soil_type in soil_types:
        color = next(ccycle)
        ks_value = next(lcycle)
        output_dir = base_dir / f"hillslope_{soil_type}"
        output_dir.mkdir(parents=True, exist_ok=True)
        params = dataclasses.replace(
            base_params, soil_type=soil_type, output_directory=output_dir
        )

        new_cvg_log = output_dir / convergence_log
        if rerun_experiments:
            run_coupled_simulation(params)
            convergence_log.rename(new_cvg_log)

        convergence_factors = compute_convergence_factors(new_cvg_log)
        t = np.arange(len(convergence_factors)) * params.dt
        ax.scatter(
            t / 60,
            convergence_factors,
            color=color,
            marker=".",
            label=f"$CR_n$ — {ks_value}",
        )

        _, capacity, conductivity = load_data(params)
        mean_capacity = capacity.mean(axis=(1, 2))
        mean_conductivity = conductivity.mean(axis=(1, 2))
        max_capacity = capacity.max()
        max_conductivity = conductivity.max()
        print(f"Soil Type: {soil_type}")
        print(f"Max observed c, K: {float(max_capacity) = :.3g}, {float(max_conductivity) = :.3g}")
        S_mean = np.zeros_like(mean_capacity)
        for index in range(len(mean_capacity)):
            S_mean[index] = compute_coupling_behavior(
                mean_conductivity[index],
                mean_capacity[index],
                params.Lz.value,
                params.dt,
                params.Mz,
            )[1]
        t = np.linspace(
            params.t_0, params.t_end, int(params.N / params.output_period) + 1
        )

        ax.scatter(
            t[1:] / 60,
            abs(S_mean)[1:],
            color=color,
            marker="+",
            label=rf"$|S|$ — {ks_value}",
        )
        ax.ticklabel_format(style="sci", axis="y", scilimits=(0, 0), useMathText=True)

        h, v = load_surface(params)
        outflow = (h * v)[:, 0]
        ax2.plot(t / 60, outflow, color=color, label=ks_value)

    ax.set(
        xlabel="Time [min]",
        ylabel="Convergence Factor",
        yscale="log",
    )
    ax.legend(loc=(0.3, 0.45))
    ax2.set(
        xlabel="Time [min]",
        ylabel="Outflow $[m^2 s^{-1}]$",
    )
    ax2.legend()

    fig.set(figwidth=5, figheight=4)
    fig2.set(figwidth=5, figheight=4)
    fig.savefig(plotting_dir / "coupled_hillslope_convergence.pdf", bbox_inches="tight")
    fig2.savefig(plotting_dir / "coupled_hillslope_outflow.pdf", bbox_inches="tight")


def silt_loam_experiment(rerun_experiment=True, low_qr=False):
    if low_qr:
        append = "_qr_low"
    else:
        append = ""
    fig, ax = plt.subplots(layout="constrained")
    fig2, ax2 = plt.subplots(layout="constrained")
    output_dir = Path(f"output/hillslope_loam{append}")
    output_dir.mkdir(parents=True, exist_ok=True)
    params = get_hillslope_params(
        N=3600,
        output_period=36,
        soil_type="loam",
        output_directory=output_dir,
        min_iterations=2,
    )
    convergence_log = Path("precice-Surface-convergence.log")

    new_cvg_log = output_dir / convergence_log
    if rerun_experiment:
        run_coupled_simulation(params, low_qr)
        convergence_log.rename(new_cvg_log)

    convergence_factors = compute_convergence_factors(new_cvg_log)
    t = np.arange(len(convergence_factors)) * params.dt
    ax.scatter(t / 60, convergence_factors, marker=".", label="$CR_n$", color="k")

    _, capacity, conductivity = load_data(params)
    mean_capacity = capacity.mean(axis=(1, 2))
    mean_conductivity = conductivity.mean(axis=(1, 2))
    max_capacity = capacity.max()
    max_conductivity = conductivity.max()
    print(f"Max observed c, K: {float(max_capacity) = :.3g}, {float(max_conductivity) = :.3g}")
    S_mean = np.zeros_like(mean_capacity)
    for index in range(len(mean_capacity)):
        S_mean[index] = compute_coupling_behavior(
            mean_conductivity[index],
            mean_capacity[index],
            params.Lz.value,
            params.dt,
            params.Mz,
        )[1]
    t = np.linspace(params.t_0, params.t_end, int(params.N / params.output_period) + 1)

    ax.scatter(
        t[1:] / 60,
        abs(S_mean)[1:],
        marker="+",
        color="tab:blue",
        label=r"$|S|$",
    )
    ax.ticklabel_format(style="sci", axis="y", scilimits=(0, 0), useMathText=True)

    h, v = load_surface(params)
    outflow = (h * v)[:, 0] * 60
    ax2.plot(t / 60, outflow, color="k")

    ax.set(
        xlabel="Time [min]",
        ylabel="Convergence Factor",
        yscale="log",
    )
    ax.legend()
    ax2.set(
        xlabel="Time [min]",
        ylabel="Outflow $[m^2 / min]$",
    )

    fig.set(figwidth=5, figheight=4)
    fig2.set(figwidth=5, figheight=4)
    fig.savefig(
        plotting_dir / f"coupled_hillslope_convergence_loam{append}.pdf",
        bbox_inches="tight",
    )
    fig2.savefig(
        plotting_dir / f"coupled_hillslope_outflow_loam{append}.pdf",
        bbox_inches="tight",
    )


silt_loam_experiment()
silt_loam_experiment(low_qr=True)
sandy_loam_experiments()
