import numpy as np
import precice
import ufl
from dune.fem.function import gridFunction
from dune.fem.scheme import galerkin as galerkin_scheme
from dune.fem.space import lagrange
from dune.fem.view import geometryGridView
from dune.grid import structuredGrid
from dune.ufl import Constant, DirichletBC

from coupling.coupling_layer import GroundCouplingLayer
from coupling.params import Params, write_output


class GroundInitialCondition:
    """Interface for (spatially varying) initial conditions for `Ground`."""

    def get_initial_condition(self, space):
        raise NotImplementedError


class LinearProfile(GroundInitialCondition):
    def __init__(self, water_table, params):
        self.water_table = water_table
        self.Lz = params.Lz.value

    def get_initial_condition(self, space):
        z = ufl.SpatialCoordinate(space)[1]
        t = self.Lz - self.water_table
        return -z + t


class GroundDirichletBC:
    """
    Interface for Dirichlet boundary bonditions that can be injected into `Ground`.

    Both spatially and time-varying boundary conditions are supported.
    """

    def get_dirichlet_bc(self, space, t) -> list[DirichletBC]:
        raise NotImplementedError


class Ground:
    """Subsurface solver class."""

    def __init__(
        self,
        params: Params,
        initial_condition: GroundInitialCondition,
        dirichlet_condition: GroundDirichletBC,
        coupling_layer: GroundCouplingLayer | None = None,
        coord_transform: callable = lambda x: x,
    ):
        self.Mx = params.Mx
        self.Mz = params.Mz
        self.Lx = params.Lx
        self.Lz = params.Lz
        self.dt = Constant(params.dt, name="dt")
        self.soil = params.soil
        self.time = Constant(params.t_0, name="t")
        self._time_checkpoint = params.t_0

        # grids and spaces
        cartGridView = structuredGrid(
            [0, 0], [self.Lx.value, self.Lz.value], [self.Mx, self.Mz]
        )

        lagSpace = lagrange(cartGridView, order=1, dimRange=cartGridView.dimension)
        space = lagrange(cartGridView, order=1)
        self.coords = ufl.SpatialCoordinate(lagSpace)
        self.x, self.z = ufl.SpatialCoordinate(lagSpace)
        initial_profile = initial_condition.get_initial_condition(self.z)
        psi_h = space.interpolate(initial_profile, name="psi_h")
        position = lagSpace.interpolate(coord_transform(self.coords), name="position")

        self.grid = geometryGridView(position)
        self.space = lagrange(self.grid, order=1)
        self.psi_h = self.space.interpolate(0, name="psi_h")
        self.psi_h.as_numpy[:] = psi_h.as_numpy

        self.psi_h_n = self.psi_h.copy(name="psi_n")
        self._psi_checkpoint = self.psi_h.copy()

        psi = ufl.TrialFunction(self.space)
        phi = ufl.TestFunction(self.space)

        mass_term = ufl.dot(
            self.theta(psi, self.x) - self.theta(self.psi_h_n, self.x), phi
        )
        flux = self.K(psi, self.x) * ufl.grad(psi + self.z)
        stiffness_term = self.dt * ufl.inner(flux, ufl.grad(phi))
        bilinear_form = (mass_term + stiffness_term) * ufl.dx

        dirichlet_conditions = dirichlet_condition.get_dirichlet_bc(
            self.space, self.time
        )
        if coupling_layer is not None:
            coupling_bc = coupling_layer.get_boundary_condition(self.grid, self.space)
            dirichlet_conditions.append(coupling_bc)

        linear_solver_params = {
            "linear.preconditioning.method": "sor",
        }
        self.scheme = galerkin_scheme(
            [bilinear_form == 0, *dirichlet_conditions],
            solver="gmres",
            parameters=linear_solver_params,
        )

        sfc_unit_normal = self._get_surface_normal(params.surface_slope)
        self.sfc_normal_flux = gridFunction(
            ufl.replace(ufl.dot(flux, sfc_unit_normal), {psi: self.psi_h}),
            self.grid,
            name="flux",
        )
        self.hydraulic_conductivity = gridFunction(
            ufl.replace(self.K(psi, self.x), {psi: self.psi_h}),
            self.grid,
            name="conductivity",
        )
        self.hydraulic_capacity = gridFunction(
            ufl.replace(self.C(psi, self.x), {psi: self.psi_h}),
            self.grid,
            name="capacity",
        )

        self.time_step = 0
        self.output_period = params.output_period
        self._vtk_output = self.grid.sequencedVTK(
            str(params.output_directory / "richards_"),
            pointdata={
                "psi": self.psi_h,
                "conductivity": self.hydraulic_conductivity,
                "capacity": self.hydraulic_capacity,
            },
            cellvector={
                "sfc_normal_flux": self.sfc_normal_flux,
            },
        )
        write_output(self.time_step, self.output_period, self._vtk_output)

    def Se(self, psi, x):
        nVG = self.soil.nVG(x)
        m = 1 - 1 / nVG
        return (1 + (-self.soil.alpha(x) * psi) ** nVG) ** (-m)

    def theta(self, psi, x):
        return ufl.conditional(
            psi <= 0,
            self.soil.theta_R(x)
            + (self.soil.theta_S(x) - self.soil.theta_R(x)) * self.Se(psi, x),
            self.soil.theta_S(x),
        )

    def K(self, psi, x):
        m = 1 - 1 / self.soil.nVG(x)
        S = self.Se(psi, x)
        return ufl.conditional(
            psi <= 0,
            self.soil.Ks(x) * S**0.5 * (1 - (1 - S ** (1 / m)) ** m) ** 2,
            self.soil.Ks(x),
        )

    def C(self, psi, x):
        """compute hydraulic capacity (d theta/d psi)"""
        alpha = self.soil.alpha(x)
        nVG = self.soil.nVG(x)
        theta_S = self.soil.theta_S(x)
        theta_R = self.soil.theta_R(x)
        C = (
            alpha
            * (theta_S - theta_R)
            * (nVG - 1)
            * (-alpha * psi) ** (nVG - 1)
            * (1 + (-alpha * psi) ** nVG) ** (1 / nVG - 2)
        )
        return ufl.conditional(psi <= 0, C, 0)

    def _get_surface_normal(self, surface_slope: float):
        """Compute the unit vector normal to the ground surface."""
        n = np.array([-surface_slope, 1])
        return ufl.as_vector(n / np.linalg.norm(n))

    def step(self) -> None:
        self.psi_h_n.assign(self.psi_h)
        self.scheme.solve(target=self.psi_h)
        self.time.value += self.dt.value

    def end_time_step(self) -> None:
        self.time_step += 1
        write_output(self.time_step, self.output_period, self._vtk_output)

    def save_state(self) -> None:
        self._time_checkpoint = self.time.value
        self._psi_checkpoint.assign(self.psi_h)

    def load_state(self) -> None:
        self.time.value = self._time_checkpoint
        self.psi_h.assign(self._psi_checkpoint)


def standalone_ground(
    params: Params,
    initial_condition: GroundInitialCondition,
    boundary_condition: GroundDirichletBC,
    *args,
):
    ground = Ground(params, initial_condition, boundary_condition, None, *args)
    while ground.time.value < params.t_end:
        ground.step()
        ground.end_time_step()


def simulate_ground(
    params: Params,
    coupling_layer: GroundCouplingLayer,
    initial_condition: GroundInitialCondition,
    boundary_condition: GroundDirichletBC,
    *args,
):
    ground = Ground(
        params, initial_condition, boundary_condition, coupling_layer, *args
    )
    participant_name = "Ground"
    solver_process_index = 0
    solver_process_size = 1
    participant = precice.Participant(
        participant_name,
        str(params.precice_config),
        solver_process_index,
        solver_process_size,
    )

    gnd_mesh_name = "Ground-Mesh"
    sfc_mesh_name = "Surface-Mesh"

    gnd_vertex_ids = participant.set_mesh_vertices(gnd_mesh_name, params.positions_gnd)

    if participant.requires_initial_data():
        participant.write_data()
    participant.initialize()

    sfc_vertex_ids, _ = participant.get_mesh_vertex_ids_and_coordinates(sfc_mesh_name)

    while participant.is_coupling_ongoing():
        if participant.requires_writing_checkpoint():
            ground.save_state()

        dt = participant.get_max_time_step_size()
        ground.dt.value = dt
        coupling_layer.river_height = participant.read_data(
            gnd_mesh_name, "h", gnd_vertex_ids, dt
        )
        ground.step()
        interface_flux = coupling_layer.mapped_interface_flux(ground.sfc_normal_flux)

        participant.write_data(sfc_mesh_name, "v", sfc_vertex_ids, interface_flux)

        participant.advance(dt)

        if participant.requires_reading_checkpoint():
            ground.load_state()
        else:
            ground.end_time_step()

    participant.finalize()
