import numpy as np
import precice
import ufl
from dune.fem.space import finiteVolume
from dune.femdg.rk import ImplEuler
from dune.grid import structuredGrid
from dune.ufl import Constant

from coupling.coupling_layer import KinWaveCouplingLayer
from coupling.params import Params, write_output
from coupling.surface_utils import MolOperator


class SurfaceInitialCondition:
    """Interface for (spatially varying) initial conditions for `Surface`."""

    def get_initial_condition(self, space):
        raise NotImplementedError


class GaussianBump(SurfaceInitialCondition):
    def __init__(self, params: Params) -> None:
        self.Lx = params.Lx

    def get_initial_condition(self, space):
        x = ufl.SpatialCoordinate(space)[0]
        a = Constant(0.5, "a")
        b = Constant(self.Lx / 2, "b")
        c = Constant(0.2, "c")
        gaussian_bump = 1.0 + a * ufl.exp(-((x - b) ** 2) / (2 * c**2))
        initial_state = ufl.as_vector([gaussian_bump])
        return initial_state


class FlatSurface(SurfaceInitialCondition):
    def __init__(self, height: float = 1.0) -> None:
        self.h = Constant(height, "h")

    def get_initial_condition(self, space):
        return ufl.as_vector([self.h])


class SurfaceBoundaryCondition:
    """Interface for boundary conditions for `Surface`.

    Does not allow for space-/time-dependent boundary conditions,
    only maps the unknowns to a new vector.
    """

    def get_boundary_condition(self, u, x):
        raise NotImplementedError


class Outflow(SurfaceBoundaryCondition):
    def get_boundary_condition(self, u, x):
        return u


class OneSidedOutflow(SurfaceBoundaryCondition):
    def __init__(self, params: Params) -> None:
        self.Lx = params.Lx.value

    def get_boundary_condition(self, u, x):
        return ufl.conditional(x[0] < 1e-8, u, 0)


class KinematicWave:
    """Class providing fluxes for solving the kinematic wave equation with the local Lax-Friedrichs flux."""

    def __init__(self, params: Params) -> None:
        self.Sf = Constant(abs(params.surface_slope), "Sf")
        self.n = Constant(params.mannings_coefficient, "n")
        self.sign = -np.sign(params.surface_slope)

    def _height(self, h):
        return ufl.Max(h, 0.0)

    def velocity(self, h):
        return self._height(h) ** (2 / 3) * ufl.sqrt(self.Sf) / self.n

    def analytical_flux(self, h):
        u = self.sign * self.velocity(h)
        return self._height(h) * u

    def _max_wave_speed(self, h):
        max_wave_speed = 5 / 3 * ufl.sqrt(self.Sf) / self.n * self._height(h) ** (2 / 3)
        return max_wave_speed

    def numerical_flux(self, n: ufl.FacetNormal, h_left, h_right):
        flux = (
            self.analytical_flux(h_left) * n[0] + self.analytical_flux(h_right) * n[0]
        )

        wave_speed_left = self._max_wave_speed(h_left)
        wave_speed_right = self._max_wave_speed(h_right)
        wave_speed = ufl.Max(wave_speed_left, wave_speed_right)
        flux += wave_speed * (h_left - h_right)

        flux *= 0.5
        return flux


class KinematicWaveSurface:
    """Surface flow solver using the KinematicWave model."""

    def __init__(
        self,
        params: Params,
        boundary_condition: SurfaceBoundaryCondition,
        initial_condition: SurfaceInitialCondition,
        source_term: callable = None,
    ):
        self.dt = Constant(params.dt, "river_timeStep")
        self.time = Constant(params.t_0, "time")
        self._time_checkpoint = self.time.value
        true_length = np.sqrt(1 + params.surface_slope**2) * params.Lx
        grid = structuredGrid([0], [true_length], [params.Mx])
        self.space = finiteVolume(grid)

        h = ufl.TrialFunction(self.space)
        v = ufl.TestFunction(self.space)
        n = ufl.FacetNormal(self.space)
        self.x = ufl.SpatialCoordinate(self.space)

        initial_state = initial_condition.get_initial_condition(self.space)
        self.state = self.space.interpolate(initial_state, name="solution")
        self.shallow_water_flux = self.state.copy()
        self._state_checkpoint = self.state.copy()

        model = KinematicWave(params)

        def F_c(h):
            return model.analytical_flux(h)

        def H_int(h):
            return model.numerical_flux(n("+"), h("+"), h("-"))

        def H_ext(h, x):
            return model.numerical_flux(
                n, h, boundary_condition.get_boundary_condition(h, x)
            )

        element_integral = -ufl.inner(F_c(h), ufl.grad(v)[0]) * ufl.dx
        if source_term is not None:
            element_integral -= source_term(self.time, self.x) * v * ufl.dx
        facet_integral = ufl.inner(H_int(h), ufl.jump(v)) * ufl.dS  # interior skeleton
        facet_integral += ufl.inner(H_ext(h, self.x), v) * ufl.ds  # domain boundary
        weak_form = -(element_integral + facet_integral)

        operator = MolOperator(weak_form, self.space, self.dt.value)
        self.time_stepper = ImplEuler(operator, cfl=1, useScipy=True)

        self.time_step = 0
        self.output_period = params.output_period
        self._vtk_output = grid.sequencedVTK(
            str(params.output_directory / "surface_"),
            pointdata={
                "height": self.state,
                "velocity": model.velocity(self.state),
                "outflow": self.state * model.velocity(self.state) * 60,
            },
        )
        write_output(self.time_step, self.output_period, self._vtk_output)

    def step(self, source_term: np.ndarray | None = None) -> None:
        self.time_stepper(self.state, self.dt.value)
        if source_term is not None:
            source = self.space.function("source_term", dofVector=source_term)
            self.state.axpy(-self.dt.value, source)
        self.time.value += self.dt.value

    def end_time_step(self) -> None:
        self.time_step += 1
        write_output(self.time_step, self.output_period, self._vtk_output)

    def save_state(self) -> None:
        self._time_checkpoint = self.time.value
        self._state_checkpoint.assign(self.state)

    def load_state(self) -> None:
        self.time.value = self._time_checkpoint
        self.state.assign(self._state_checkpoint)


def standalone_surface(
    params: Params,
    initial_condition: SurfaceInitialCondition,
    boundary_condition: SurfaceBoundaryCondition,
    *args,
):
    surface = KinematicWaveSurface(params, boundary_condition, initial_condition, *args)
    while surface.time.value < params.t_end:
        surface.step()
        surface.end_time_step()


def simulate_surface(
    params: Params,
    initial_condition: SurfaceInitialCondition,
    boundary_condition: SurfaceBoundaryCondition,
    coupling_layer: KinWaveCouplingLayer,
    *args,
):
    surface = KinematicWaveSurface(params, boundary_condition, initial_condition, *args)
    participant_name = "Surface"
    solver_process_index = 0
    solver_process_size = 1
    participant = precice.Participant(
        participant_name,
        str(params.precice_config),
        solver_process_index,
        solver_process_size,
    )

    sfc_mesh_name = "Surface-Mesh"
    gnd_mesh_name = "Ground-Mesh"

    sfc_vertex_ids = participant.set_mesh_vertices(sfc_mesh_name, params.positions_sfc)

    if participant.requires_initial_data():
        participant.write_data()
    participant.initialize()

    gnd_vertex_ids, _ = participant.get_mesh_vertex_ids_and_coordinates(gnd_mesh_name)

    while participant.is_coupling_ongoing():
        if participant.requires_writing_checkpoint():
            surface.save_state()
        dt = participant.get_max_time_step_size()
        surface.dt.value = dt
        coupling_layer.source_term = participant.read_data(
            sfc_mesh_name, "v", sfc_vertex_ids, dt
        )

        surface.step(coupling_layer.source_term)

        river_height = coupling_layer.mapped_river_height(surface.state)
        participant.write_data(gnd_mesh_name, "h", gnd_vertex_ids, river_height)

        dt = participant.advance(dt)

        if participant.requires_reading_checkpoint():
            surface.load_state()
        else:
            surface.end_time_step()

    participant.finalize()
