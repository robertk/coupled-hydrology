# Coupled surface & subsurface flows

Python code to simulate coupled surface-subsurface flow.
This implementation is designed to study different coupling algorithms.
We solve the Richards equation for subsurface flow and the shallow water equations/kinematic wave equations for surface flow with [DUNE](https://www.dune-project.org/).
The solvers are coupled with [preCICE 3](http://precice.org/).

## Overview & Usage

### Standard Setup

The standard setup consists of two packages:
- `coupling` contains solvers and coupling routines for nonlinear 2D-1D subsurface-surface problems
- `linear_model` contains solvers, coupling routines, and routines to compute analytical results for the coupled linear problem

To install and use these, run

```
> pip install -e .
```

This will automatically install dependencies to run all scripts in these two directories.
The repository contains additional scripts for postprocessing, tests, and code formatting.
To install the required dependencies for these, use the [dependency specifiers](https://packaging.python.org/en/latest/specifications/dependency-specifiers/#dependency-specifiers) `[postprocess],[test],[dev]`, respectively.
Alternatively, run

```
> pip install -e ".[all]"
```

The standard setup enables creating and running your own Python scripts to run experiments/example cases.
The provided scripts in `experiments` make use of the postprocessing routines.
If the package is installed correctly, you can run these examples using, e.g.,

```
> python experiments/drainage_trench/coupled_drainage_trench.py
```

### Reproduce Results

To reproduce results, we recommend setting up a virtual environment with the package versions given in the `requirements.txt`.
Specifically,

```bash
> python3.12 -m venv .venv
> source .venv/bin/activate
> pip install -r requirements.txt
> pip install ".[all]"
```

## Development Guidelines

### Tests

We use [pytest](https://docs.pytest.org/) for testing.
To run the test suite, start by installing the package locally:

```bash
> pip install --user -e ".[all]" # (optional) flags for a local, editable installation
> pytest
```

### Code formatting

We use [black](https://black.readthedocs.io/en/stable/), [isort](https://pycqa.github.io/isort/), and [flake8](https://flake8.pycqa.org/en/latest/) for code formatting and linting.
To use all three at once, run the provided formatting script:

```bash
> source ./format_code.sh
```
