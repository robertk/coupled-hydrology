#!/usr/bin/env bash

echo "Cleaning..."

rm -fv coupling/precice-config.xml
rm -fv precice-*.log
rm -rfv precice-run

echo "Cleaning complete!"